package ictgradschool.industry.lab08.ex01;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class ExerciseOne {

    public void start() {

//        printNumEsWithFileReader();

        printNumEsWithBufferedReader();

    }

    private void printNumEsWithFileReader() {

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a FileReader.

        int total = 0;
        int numE = 0;
        int var = 0;

        FileReader fR = null;
        try {fR = new FileReader("input1.txt");
            while (var != -1) {
                var = fR.read();
                total ++;
                if (var == 'e' || var == 'E') {
                    numE++;
                }
            }
        } catch(IOException e) {
            System.out.println("IO problem");
        }

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    private void printNumEsWithBufferedReader() {
        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a BufferedReader.

        int total = 0;
        int numE = 0;
        String var = "";


        try {
            File fR = new File("input1.txt");
            BufferedReader reader = new BufferedReader(new FileReader(fR));
            while (var != null) {
                var = reader.readLine();
                total = var.length();
                for (int i = 0; i < var.length(); i++) {
                    if (var.charAt(i) == ('e') || var.charAt(i) == ('E')) {
                        numE++;
                    }
                }
            }
        } catch(IOException e) {
            System.out.println("IO problem");
        } catch (NullPointerException e) {
            System.out.println("you've reached the end of the file");
        }

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    public static void main(String[] args) {
        new ExerciseOne().start();


//            int num = 0;
//            FileReader fR = null;
//            try {
//                fR = new FileReader("input1.txt");
//                num = fR.read();
//                System.out.println(num);
//                // to read as char need to cast to char see below line...
//                System.out.println((char)fR.read());
//                System.out.println(fR.read());
//                System.out.println(fR.read());
//                System.out.println(fR.read());
//                fR.close();
//            } catch(IOException e) {
//                System.out.println("IO problem");
//            }
//        }


}}
