package ictgradschool.industry.lab08.ex04;

import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieWriter extends MovieWriter {

    @Override
    protected void saveMovies(String fileName, Movie[] films) {

        // TODO Implement this with a PrintWriter

        File myFile = new File(fileName);
        try (PrintWriter writer = new PrintWriter(new FileWriter(myFile))) {
            for (int i = 0; i < films.length ; i++) {
                writer.println(films[i].toString().replace(", " , ","));
            }
        }
        catch (IOException e) {
            System.out.println(e);
        }
    }

    public static void main(String[] args) {
        new Ex4MovieWriter().start();
    }

}
