package ictgradschool.industry.lab08.ex04;

import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieReader;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieReader extends MovieReader {

    @Override
    protected Movie[] loadMovies(String fileName) {

        // TODO Implement this with a Scanner

        File myFile = new File(fileName);
        try (Scanner scanner = new Scanner(myFile)) {

            scanner.useDelimiter(",|\\r\\n");


            while (scanner.hasNext()) {


                String a = scanner.next();
                String b = scanner.next();
                String c = scanner.next();

                System.out.println(a + " " + b + " " + c);
            }

        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }


        return null;
    }

    public static void main(String[] args) {
        new Ex4MovieReader().start();
    }
}
